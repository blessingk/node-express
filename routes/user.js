var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');
var userController = require('../controllers/user_controller');

var csrfProtection = csrf();
router.use(csrfProtection);

router.get('/profile', isLoggedIn, userController.getProfile);

router.get('/logout', isLoggedIn, userController.logoutUser);

router.use('/', notLoggedIn, function (req, res, next) {
    next();
})

router.get('/signup/', userController.getSignUpPage);

router.post('/signup/', passport.authenticate('local.signup', {
    failureRedirect: '/user/signup',
    failureFlash: true
}), function (req, res, next){
    if(req.session.oldUrl) {
        var oldUrl = req.session.oldUrl
        req.session.oldUrl = null;
        return res.redirect(oldUrl);
    }
    return res.redirect('/user/profile');
});

router.get('/signin', userController.getSignInPage);

router.post('/signin', passport.authenticate('local.signin', {
    failureRedirect: '/user/signin',
    failureFlash: true
}), function (req, res, next){
    if(req.session.oldUrl) {
        var oldUrl = req.session.oldUrl
        req.session.oldUrl = null;
        return res.redirect(oldUrl);
    }
    return res.redirect('/user/profile');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if(!req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}
