var express = require('express');
var router = express.Router();

var productController = require('../controllers/product_controller');
var cartController = require('../controllers/cart_controller');
var paymentController = require('../controllers/payment_controller');

/* GET home page. */
router.get('/', productController.getProducts);

router.get('/add-to-cart/:id', cartController.addToCart);

router.get('/shopping-cart/', cartController.getShoppingCart);

router.get('/decrease/cart/item/:id', cartController.decreaseCartItem);

router.get('/increase/cart/item/:id', cartController.increaseCartItem);

router.get('/remove/cart/items/:id', cartController.removeCartItems);

router.get('/checkout', isLoggedIn, cartController.getCartCheckout);

router.post('/checkout', paymentController.stripePayment);


module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()){
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin/');
}
