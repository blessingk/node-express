var Product = require('../models/products');

var products = [
    new Product({
        imagePath: 'https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png',
        title: "New Product",
        description: "Awesome Product please try it",
        price: 20
    }),

    new Product({
        imagePath: 'https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png',
        title: "New Product 1",
        description: "Awesome Product please try it 1",
        price: 20
    }),

    new Product({
        imagePath: 'https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png',
        title: "New Product 2",
        description: "Awesome Product please try it 2",
        price: 20
    }),

    new Product({
        imagePath: 'https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png',
        title: "New Product 3",
        description: "Awesome Product please try it 3",
        price: 20
    }),

    new Product({
        imagePath: 'https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png',
        title: "New Product 4",
        description: "Awesome Product please try it 4",
        price: 20
    }),
];
var done = 0;
for (var i = 0; i < products.length; i++){
    products[i].save(function (err, result) {
        done++;
        if(done === products.length){
            exit();
        }
    });
}

function exit(){
    mongoose.disconnect();
}

