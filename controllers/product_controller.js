var Product = require('../models/products');

exports.getProducts = function(req , res) {
    var successMessage = req.flash('success')[0];
    return Product.find(function (err, products) {
        res.render('shop/index', { title: 'Shopping Cart', products: products, successMessage: successMessage, noMessage: !successMessage });
    });
}


