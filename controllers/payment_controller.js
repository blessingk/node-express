var Cart = require('../models/cart');
var Order = require('../models/order');

exports.stripePayment = function (req, res, next) {
    if(!req.session.cart) {
        return res.redirect('/shopping-cart');
    }
    var cart = new Cart(req.session.cart);

    var stripe = require('stripe')(
        'sk_test_51H9kbJII7NwW1kB9IyOmFx1h5Mo4Nkt2Nx3qr50i8C0tON1x71n6JdqihA51wxwCbkY3HqqMIWeJXWDplHd8EAVz003BWIS3Ve'
    );

    stripe.charges.create({
        amount: cart.totalPrice * 10,
        currency: 'usd',
        source: req.body.stripeToken,
        description: 'charge for '+req.body.card_name
    }, function (err, charge) {
        if(err) {
            req.flash('error', err.message);
            return res.redirect('/checkout');
        }

        var order = new Order({
            user: req.user,
            cart: req.session.cart,
            address: req.body.address,
            name: req.body.name,
            paymentId: charge.id
        });

        order.save(function (err, result) {
            req.flash('success', 'Payment was successful');
            req.session.cart = null;
            res.redirect('/');
        })

    });
}
