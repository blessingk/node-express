var passport = require('passport');
var Cart = require('../models/cart');
var Order = require('../models/order');

exports.getSignInPage = function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/signin', { csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0 });
};

exports.getSignUpPage = function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/signup', { csrfToken: req.csrfToken(), messages: messages, hasErrors: messages.length > 0 });
};

exports.logoutUser = function (req, res, next) {
    req.logout();
    res.redirect('/');
};

exports.getProfile = function (req, res, next) {
    Order.find({user: req.user}, function (err, orders) {
        if(err) {
            return res.wrire("Error in trying to get orders.")
        }
        var cart;
        orders.forEach(function (order) {
            cart = new Cart(order.cart);
            order.items = cart.generateArray();
        });

        res.render('user/profile', {orders: orders});
    })
};

exports.userSignUp = function (req, res, next) {
    passport.authenticate('local.signup', {
        successRedirect: '/user/profile',
        failureRedirect: '/user/signup',
        failureFlash: true
    });
};

exports.userSignIn = function (req, res, next) {
    passport.authenticate('local.signin', {
        successRedirect: '/user/profile',
        failureRedirect: '/user/signin',
        failureFlash: true
    });
};
