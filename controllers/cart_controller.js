var Cart = require('../models/cart');
var Product = require('../models/products')
var User = require('../models/user');

exports.addToCart = function(req , res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if(err) {
            return res.redirect('/');
        }

        cart.add(product, product.id);
        req.session.cart = cart;
        res.redirect('/');
    });
};

exports.getShoppingCart = function (req, res, next) {
    if (!req.session.cart) {
        return res.render('shop/shopping_cart', {products: null})
    }
    var cart = new Cart(req.session.cart);
    res.render('shop/shopping_cart', {products: cart.generateArray(), cartTotal: cart.totalPrice})
};

exports.getCartCheckout = function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping_cart/')
    }
    var cart = new Cart(req.session.cart);
    var errMsg = req.flash('error')[0];
    res.render('shop/checkout', {products: cart.generateArray(), cartTotal: cart.totalPrice, errMessage: errMsg, noErrors: !errMsg})
};

exports.decreaseCartItem = function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.decreaseCartItem(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
};

exports.increaseCartItem = function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.increaseCartItem(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
};

exports.removeCartItems = function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.removeCartItems(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
};


